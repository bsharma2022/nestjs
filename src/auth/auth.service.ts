import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UsersService } from 'src/users/users.service';
import { SECRET_KEY } from './constants';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
  ) {}

  async signIn(userName: string, password: string): Promise<string> {
    const user = this.usersService.findOne(userName);

    if (user?.userName !== userName || user?.password !== password) {
      throw new UnauthorizedException();
    }

    const payload = { userName: user.userName };
    const token = await this.jwtService.signAsync(payload, {
      secret: SECRET_KEY,
    });
    return token;
  }
}
