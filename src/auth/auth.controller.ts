import { Body, Controller, Post } from '@nestjs/common';
import { AuthService } from './auth.service';

export interface SignInDto {
  userName: string;
  password: string;
}

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('login')
  async userSignIn(@Body() signInDto: SignInDto) {
    try {
      const token = await this.authService.signIn(
        signInDto.userName,
        signInDto.password,
      );

      return {
        status: 'success',
        token,
      };
    } catch (error) {
      return error;
    }
  }
}
