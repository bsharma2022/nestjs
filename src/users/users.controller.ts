import { Controller, Get, Request, UseGuards } from '@nestjs/common';
import { UsersService } from './users.service';
import { AuthGuard } from '@nestjs/passport';

@Controller('users')
@UseGuards(AuthGuard('jwt'))
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Get()
  getAllUsers() {
    return this.usersService.getAllUserData();
  }

  @Get('profile')
  getLoggedInUser(@Request() req) {
    const { userName } = req.user;

    return this.usersService.getCurrentUser(userName);
  }
}
