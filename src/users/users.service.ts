import { Injectable } from '@nestjs/common';

@Injectable()
export class UsersService {
  private userList = [
    {
      id: 1,
      name: 'Bhavesh Sharma',
      age: 22,
      userName: 'bsharma2022',
      password: 'bhavesh@123',
    },
    {
      id: 2,
      name: 'Sourabh sugandhi',
      age: 23,
      userName: 'ssugandhi2022',
      password: 'sourabh@123',
    },
  ];

  getAllUserData() {
    return this.userList;
  }

  getCurrentUser(userName) {
    return this.findOne(userName);
  }

  findOne(userName: string) {
    return this.userList.find((user) => user.userName === userName);
  }
}
